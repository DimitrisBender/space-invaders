#include <allegro5/allegro.h>
#include <allegro5/allegro_native_dialog.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <cstdlib>
#include <ctime>
#include "objects.h"

// CONSTANTS
#define WIDTH			640
#define HEIGHT			800
#define PLAYER_COLOR	0, 255, 0
#define PLAYER_WIDTH	56
#define PLAYER_HEIGHT	19
#define ENEMY_WIDTH		32
#define ENEMY_HEIGHT	16
#define BULLET_RADIUS	4
#define SPEED_INC		0.15
// INIT DEFINES
#define INIT_PLAYERX		0
#define INIT_PLAYERY 		800
#define INIT_PLAYER_SPEED	3
#define INIT_LIVES			3
#define INIT_ENEMY_SPEED	0.5
#define MAX_ENEMY_BULLETS	20
#define MAX_PLAYER_BULLETS	6
#define ENEMY_LINES			4
#define MAX_ENEMIES_ROW		10

// GLOBALS ========
enum KEYS{LEFT, RIGHT};
bool keys[2] = {false, false};
Bullet enemyBullets[MAX_ENEMY_BULLETS];
Bullet playerBullets[MAX_PLAYER_BULLETS];
Alien aliens[ENEMY_LINES][MAX_ENEMIES_ROW];

// prototypes =====
void initializeObjects(Falcon &player);

void initPlayer(Falcon &player);
void drawPlayer(Falcon player);
void moveLeft(Falcon &player);
void moveRight(Falcon &player);

void initEnemies(Alien aliens[][MAX_ENEMIES_ROW], int cy, int cx);
void drawEnemies(Alien aliens[][MAX_ENEMIES_ROW], int cy, int cx);
void updateEnemies(Alien aliens[][MAX_ENEMIES_ROW], int cy, int cx);

void initBullets(Bullet bullets[], int size);
void drawBullets(Bullet bullets[], int size);
void updateBullets(Bullet bullets[], int size);
void fireBullet(Bullet bullets[], int size);
void shipFire(Falcon player, Bullet bullets[], int bulletSize);				// fire bullets from our spaceship
void enemyFire(Alien aliens[][MAX_ENEMIES_ROW], int, int, Bullet[], int);	// fire bullets from the enemies

bool enemyCollision(Bullet &bullet, Alien &alien);
void CollisionDetectionEnemy(Bullet bullets[], int bSize, Alien aliens[][MAX_ENEMIES_ROW], int cy, int cx, Falcon &player);
bool playerCollision(Bullet &bullet, Falcon &player);
void CollisionDetectionPlayer(Bullet bullets[], int bulletSize, Falcon &player);

int main(void)
{
	// primitive vars
	bool done = false;		// terminate execution flag
	bool pause = false;		// when user press escape pause/resume
	bool redraw = false;	// should I redraw the scene?
	int gameOver = false;
	const int FPS = 60;
	double currTime, prevTime;
	const float shootInterval = 0.5;

	// object vars
	Falcon player;

	// allegro vars
	ALLEGRO_DISPLAY *display = NULL;
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;
	ALLEGRO_TIMER *timer = NULL;
	ALLEGRO_FONT *font = NULL;
	ALLEGRO_COLOR bgColor;

	// initialize display
	if(!al_init())
	{
		al_show_native_message_box(NULL, NULL, NULL, "Could not initialize Allegro 5", NULL, 0);
		return -1;
	}

	display = al_create_display(WIDTH, HEIGHT);
	if(!display)
	{
		al_show_native_message_box(display, "Error", "Display Settings", "Display window was not created successfully", NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return -1;
	}
	// initialize al_modules
	al_init_primitives_addon();
	al_init_font_addon(); // initialize the font addon
	al_init_ttf_addon();// initialize the ttf (True Type Font) addon
	al_install_keyboard();
	bgColor = al_map_rgb(0, 0, 0);
	event_queue = al_create_event_queue();
	if(!event_queue) {
		al_show_native_message_box(display, "Error", "Display Settings", "Failed to create event queue", NULL, ALLEGRO_MESSAGEBOX_ERROR);
	    al_destroy_display(display);
	    return -1;
	}
	font = al_load_ttf_font("chintzys.ttf", 20.0, 0);
	if (!font){
		fprintf(stderr, "Could not load 'chintzys.ttf'.\n");
		return -1;
	}
	timer = al_create_timer(1.0 / FPS);
	al_register_event_source(event_queue, al_get_keyboard_event_source());
	al_register_event_source(event_queue, al_get_display_event_source(display));
	al_register_event_source(event_queue, al_get_timer_event_source(timer));

	// initialize objects
	srand(time(NULL));
	initPlayer(player);
	initEnemies(aliens, ENEMY_LINES, MAX_ENEMIES_ROW);
	initBullets(playerBullets, MAX_PLAYER_BULLETS);
	initBullets(enemyBullets, MAX_ENEMY_BULLETS);


	drawPlayer(player);
	al_flip_display();
	al_clear_to_color(bgColor);
	al_start_timer(timer);
	prevTime = time(NULL);
	// GAME LOOP
	while(!done)
	{
		ALLEGRO_EVENT ev;
     	al_wait_for_event(event_queue, &ev);

     	if (ev.type == ALLEGRO_EVENT_KEY_DOWN)
     	{
     		switch(ev.keyboard.keycode)
     		{
     			case ALLEGRO_KEY_ESCAPE:	// pause game
     				pause = !pause;
     				break;
 				case ALLEGRO_KEY_LEFT:		// steer left
 					keys[LEFT] = true;
 					break;
 				case ALLEGRO_KEY_RIGHT:		// steer right
 					keys[RIGHT] = true;
 					break;
 				case ALLEGRO_KEY_SPACE:		// fire!
 					currTime = time(NULL);
 					if (currTime - prevTime > shootInterval)
 					{
 						shipFire(player, playerBullets, MAX_PLAYER_BULLETS);
 						prevTime = currTime;
 					}
 					break;
     		}
     	} else if (ev.type == ALLEGRO_EVENT_KEY_UP)
     	{
     		switch(ev.keyboard.keycode)
     		{
 				case ALLEGRO_KEY_LEFT:
 					keys[LEFT] = false;
 					break;
 				case ALLEGRO_KEY_RIGHT:
 					keys[RIGHT] = false;
 					break;
			}
     	} else if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)	// close window - terminate
     	{
        	done = true;
     	} else if (ev.type == ALLEGRO_EVENT_TIMER)
     	{
     		if (!pause)		// if not game paused
     		{
     			redraw = true;
     			if (keys[LEFT])
			     	moveLeft(player);
			    else if(keys[RIGHT])
			     	moveRight(player);

			    enemyFire(aliens, ENEMY_LINES, MAX_ENEMIES_ROW, enemyBullets, MAX_ENEMY_BULLETS);
		        updateBullets(enemyBullets, MAX_ENEMY_BULLETS);
		        updateBullets(playerBullets, MAX_PLAYER_BULLETS);
		        CollisionDetectionPlayer(enemyBullets, MAX_ENEMY_BULLETS, player);
		        if (player.lives <= 0) {
					gameOver = true;
				}
     		}
     	}

		if(redraw && al_is_event_queue_empty(event_queue)) {
	        redraw = false;

			if(!gameOver)
			{
				al_draw_textf(font, al_map_rgb(255,255,255), WIDTH/2, 5, ALLEGRO_ALIGN_CENTRE, "Falcon's score is %u. Remaining lives: %i", player.score, player.lives);
				drawPlayer(player);
				drawBullets(enemyBullets, MAX_ENEMY_BULLETS);
				drawBullets(playerBullets, MAX_PLAYER_BULLETS);
		        drawEnemies(aliens, ENEMY_LINES, MAX_ENEMIES_ROW);

			    updateEnemies(aliens, ENEMY_LINES, MAX_ENEMIES_ROW);
		        CollisionDetectionEnemy(playerBullets, MAX_PLAYER_BULLETS, aliens, ENEMY_LINES, MAX_ENEMIES_ROW, player);
		    }

	        al_flip_display();
	        al_clear_to_color(bgColor);
	     }
	}

	// clean up
	al_destroy_timer(timer);
	al_destroy_event_queue(event_queue);
	al_destroy_display(display);
}

void initPlayer(Falcon &player)
{
	player.ID 	 	= PLAYER;
	player.x 	 	= INIT_PLAYERX;
	player.y	 	= INIT_PLAYERY;
	player.speed 	= INIT_PLAYER_SPEED;
	player.lives 	= INIT_LIVES;
	player.score	= 0;
}

// Draw our lovely shapeship
void drawPlayer(Falcon player)
{
	al_draw_filled_triangle(player.x, player.y, (player.x + PLAYER_WIDTH/2)-1, (player.y - PLAYER_HEIGHT), (player.x + PLAYER_WIDTH), player.y, al_map_rgb(PLAYER_COLOR));
	al_draw_filled_rectangle(player.x, (player.y - PLAYER_HEIGHT/3), player.x+4, player.y, al_map_rgb(0, 0, 255));
	al_draw_filled_circle(player.x+3, (player.y - PLAYER_HEIGHT/3)-2, 2, al_map_rgb(255, 0, 0));
	al_draw_filled_rectangle((player.x + PLAYER_WIDTH/2)-3, (player.y - PLAYER_HEIGHT)-4, (player.x + PLAYER_WIDTH/2)+1, player.y, al_map_rgb(0, 0, 255));
	al_draw_filled_circle((player.x + PLAYER_WIDTH/2), (player.y - PLAYER_HEIGHT)-6, 2, al_map_rgb(255, 0, 0));
	al_draw_filled_rectangle((player.x + PLAYER_WIDTH-4), (player.y - PLAYER_HEIGHT/3), (player.x + PLAYER_WIDTH), player.y, al_map_rgb(0, 0, 255));
	al_draw_filled_circle((player.x + PLAYER_WIDTH-1), (player.y - PLAYER_HEIGHT/3)-2, 2, al_map_rgb(255, 0, 0));
	al_draw_filled_rectangle(player.x, player.y-2, player.x+PLAYER_WIDTH+1, player.y, al_map_rgb(255, 127, 127));
}

// move player functions
void moveLeft(Falcon &player)
{
	player.x -= player.speed;
	if (player.x < 0)
		player.x = 0;
}
void moveRight(Falcon &player)
{
	player.x += player.speed;
	if (player.x+PLAYER_WIDTH > WIDTH)
		player.x = WIDTH - PLAYER_WIDTH - 1;
}

// Bullet functions
void initBullets(Bullet bullets[], int size)
{
	for (int i = 0; i < size; ++i)
	{
		bullets[i].ID = BULLET;
		bullets[i].isAlive = false;
	}
}

// draw a bullet on the screen
void drawBullets(Bullet bullets[], int size)
{
	for (int i = 0; i < size; ++i)
	{
		if (bullets[i].isAlive)
		{
			al_draw_filled_ellipse(bullets[i].x, bullets[i].y, BULLET_RADIUS-1.5, BULLET_RADIUS, al_map_rgb(255,0,0));
		}
	}
}

// update bullet position
void updateBullets(Bullet bullets[], int size)
{
	for (int i = 0; i < size; ++i)
	{
		if(bullets[i].isAlive)
		{
			bullets[i].y += bullets[i].speed;
			if (bullets[i].y < 0 || bullets[i].y > HEIGHT+1)
			{
				bullets[i].isAlive = false;
			}
		}
	}
}

// player fires bullets
void fireBullet(Bullet bullets[], int size, float x, float y, float speed)
{
	for (int i = 0; i < size; ++i)
	{
		if (!bullets[i].isAlive)
		{
			bullets[i].x = x;
			bullets[i].y = y;
			bullets[i].speed = speed;
			bullets[i].isAlive = true;
			break;
		}
	}
}

void shipFire(Falcon player, Bullet bullets[], int bulletSize)
{
	fireBullet(bullets, bulletSize, player.x + PLAYER_WIDTH/2, player.y - 15, -10);
}

void enemyFire(Alien aliens[][MAX_ENEMIES_ROW], int cy, int cx, Bullet bullets[], int bulletSize)
{
	for (int i = 0; i < cy; ++i)
	{
		for (int j = 0; j < cx; ++j)
		{
			Alien a = aliens[i][j];
			if(a.isAlive) {
				if( rand() % 1000 == 0 ) {
					fireBullet(bullets, bulletSize, (a.x + ENEMY_WIDTH/2), (a.y + ENEMY_HEIGHT)+4.5, 7);
				}
			}
		}
	}

}

void initEnemies(Alien aliens[][MAX_ENEMIES_ROW], int cy, int cx)
{
	int start;
	int marginx = 50;
	int marginy = 60;

	for (int i = 0; i < cy; ++i)
	{
		start = 40;
		for (int j = 0; j < cx; ++j)
		{
			aliens[i][j].ID = ENEMY;
			aliens[i][j].x = start += marginx;
			aliens[i][j].y = marginy;
			aliens[i][j].speed = INIT_ENEMY_SPEED;
			aliens[i][j].isAlive = true;
			aliens[i][j].points = 20;
		}
		marginy += ENEMY_HEIGHT + 20;
	}
}

void drawEnemies(Alien aliens[][MAX_ENEMIES_ROW], int cy, int cx)
{
	for (int i = 0; i < cy; ++i)
	{
		for (int j = 0; j < cx; ++j)
		{
			if(aliens[i][j].isAlive) {
				al_draw_filled_rectangle(aliens[i][j].x, aliens[i][j].y, aliens[i][j].x + ENEMY_WIDTH, aliens[i][j].y + ENEMY_HEIGHT/3, al_map_rgb(127, 127, 255));
				al_draw_ellipse((aliens[i][j].x + ENEMY_WIDTH/2), (aliens[i][j].y+ENEMY_HEIGHT/2), ENEMY_WIDTH/2, ENEMY_HEIGHT/2, al_map_rgb(127, 0, 255), 2);
				al_draw_filled_rectangle((aliens[i][j].x + ENEMY_WIDTH/2)-0.5, (aliens[i][j].y), (aliens[i][j].x + ENEMY_WIDTH/2)+0.5, (aliens[i][j].y+ENEMY_HEIGHT), al_map_rgb(255, 255, 255));
			}

		}
	}
}

void levelDown(Alien aliens[][MAX_ENEMIES_ROW], int cy, int cx)
{
	for (int i = 0; i < cy; ++i)
	{
		for (int j = 0; j < cx; ++j)
		{
			aliens[i][j].y += 30;
			aliens[i][j].speed += SPEED_INC;
			aliens[i][j].speed *= -1;
		}
	}
}

void updateEnemies(Alien aliens[][MAX_ENEMIES_ROW], int cy, int cx)
{
	if ( (aliens[0][0].x < 4 && aliens[0][0].speed < 0) || 
		( ((aliens[0][cx-1].x + aliens[0][cx-1].speed + ENEMY_WIDTH) > WIDTH-4) && aliens[0][cx-1].speed > 0) )
		levelDown(aliens, cy, cx);
	else
		for (int i = cy-1; i >= 0; --i)
		{
			for (int j = cx-1; j >= 0; --j)
			{
				aliens[i][j].x += aliens[i][j].speed;
			}
		}
}

void CollisionDetectionEnemy(Bullet bullets[], int bSize, Alien aliens[][MAX_ENEMIES_ROW], int cy, int cx, Falcon &player)
{
	for (int b = 0; b < bSize; ++b)
	{
		if(bullets[b].isAlive)
		{
			for (int i = 0; i < cy; ++i)
			{
				for (int j = 0; j < cx; ++j)
				{
					Alien a = aliens[i][j];
					if(a.isAlive) {
						if (enemyCollision(bullets[b], aliens[i][j])) {
							player.score += a.points;
						}
					}
				}
			}
		}
	}
}

bool enemyCollision(Bullet &bullet, Alien &alien)
{
	if( ((bullet.x - BULLET_RADIUS-1.5) < (alien.x + ENEMY_WIDTH)) &&	// TEST RIGHT
		((bullet.y - BULLET_RADIUS) < (alien.y + ENEMY_HEIGHT)) &&		// TEST DOWN
		((bullet.x + BULLET_RADIUS-1.5) > alien.x) &&					// TEST LEFT
		((bullet.y + BULLET_RADIUS) > alien.y) ) {
		bullet.isAlive = false;
		alien.isAlive = false;
		return true;
	}
	return false;
}

bool playerCollision(Bullet &bullet, Falcon &player)
{
	if( ((bullet.x - BULLET_RADIUS-1.5) < (player.x + PLAYER_WIDTH)) &&		// TEST RIGHT
		((bullet.y - BULLET_RADIUS) < (player.y + (PLAYER_HEIGHT*(2/3))))	// TEST DOWN
		&& ((bullet.x + BULLET_RADIUS-1.5) > player.x) &&					// TEST LEFT
		((bullet.y + BULLET_RADIUS) > player.y) ) {
		bullet.isAlive = false;
		--player.lives;
		return true;
	}
	return false;
}

void CollisionDetectionPlayer(Bullet bullets[], int bulletSize, Falcon &player)
{
	for (int b = 0; b < bulletSize; ++b)
	{
		Bullet bullet = bullets[b];
		if(bullet.isAlive) {
			playerCollision(bullet, player);
		}
	}
}