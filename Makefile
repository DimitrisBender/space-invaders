CPP=g++

main.o: main.cpp objects.h
	$(CPP) -Wall -ggdb -c main.cpp

run: main.o
	$(CPP) main.o -lallegro -lallegro_font -lallegro_ttf -lallegro_dialog -lallegro_main -lallegro_primitives -o spaceinvaders
clean:
	rm ./spaceinvaders ./main.o