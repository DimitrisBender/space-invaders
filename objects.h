#ifndef __OBJECTS_H__
#define __OBJECTS_H__
#include <iostream>

enum IDs{PLAYER, BULLET, ENEMY};	// Game object identification

struct Falcon						// Our Player
{
	int ID;
	float x;
	float y;
	float speed;
	int lives;
	unsigned int score;
};

struct Bullet 						// bullet instance (both for player and enemies)
{
	int ID;
	float x;
	float y;
	float speed;
	bool isAlive;
};

struct Alien						// our enemies
{
	int ID;
	float x;
	float y;
	float speed;
	bool isAlive;
	int points;
};

#endif